document.getElementById('submit').onclick = function () {
    var oldPass = document.getElementById('oldPass').value;
    var newPass = document.getElementById('newPass').value;
    var confirmPass = document.getElementById('newPassConf').value;

    if (oldPass && newPass && confirmPass) {
        if (newPass == confirmPass)
            alert('Password changed!');
        else
            alert('The new password was wrong');
    } else {
        alert('All fields are required!');
    }
}

document.getElementById('show1').onclick = function () {
    var s = document.getElementById('oldPass');
    if (s.type == "password") {
        s.type = "text";
    } else {
        s.type = "password";
    }
}

document.getElementById('show2').onclick = function () {
    var s = document.getElementById('newPass');
    if (s.type == "password") {
        s.type = "text";
    } else {
        s.type = "password";
    }
}

document.getElementById('show3').onclick = function () {
    var s = document.getElementById('newPassConf');
    if (s.type == "password") {
        s.type = "text";
    } else {
        s.type = "password";
    }
}